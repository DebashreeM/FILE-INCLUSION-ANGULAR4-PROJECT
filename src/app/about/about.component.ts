import { Component, OnInit,AfterViewChecked, NgModule } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import {Http,Response,Headers} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import 'rxjs/add/operator/toPromise';
declare var jquery:any;
declare var $ :any;
declare function checkJS();
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  private headers = new Headers({'Content-Type':'application/json'});
  aboutData = [];
  processValidation = false;
  pattern="https?://.+";
  filePath:string;
  filelist: Array<{filename: string, intkey: string}> = [{
      filename: 'http://oditek.in/jslib/jslib.js',
      intkey: 'aboutlib'
	  },{
      filename: 'http://oditek.in/jslib/aboutjs.js',
      intkey: 'aboutjs'
  }];
  textForm = new FormGroup({
    url: new FormControl('', [Validators.required, Validators.pattern(this.pattern)])
  });
  constructor(private router:Router,private route:ActivatedRoute,private http:Http) { }
  ngAfterViewChecked() {
    $('#title').attr('style','font-weight:bold');
    /*$.getScript(this.filePath,function(){
      setTimeout(function(){
        checkJS();
      }, 5000);
    })*/
  }
  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.filelist.forEach(item => {
		let parampath=atob(params['filepath']);
        if(item.intkey==parampath)
          this.filePath = item.filename;
        else
          return;
      });
    });
    this.http.get('http://localhost:3000/articles').subscribe(
      (res:Response)=>{
        this.aboutData = res.json();
      }
    )
  }
  onTextFormSubmit(){
    this.processValidation = true;
    if (this.textForm.invalid) {
      return;
    } 
    let urlValue = this.textForm.value;
    let url = urlValue.url;
    if(url.substring(0, 16) == 'http://oditek.in' && url.substring(17, 22) == 'jslib' && url.substring(23, 31)=='jslib.js'){
      let script = document.createElement("SCRIPT");
      script.setAttribute("type", "text/javascript");
      script.setAttribute("src", url);
      script.setAttribute('integrity', 'sha384-0sRBxglEC3T0r7ccWjtw+lTY/a9U8CmQNjqlb1llFxanLerbX7OpgLdFTDL/ri48');
      script.setAttribute('crossorigin','anonymous');
      document.getElementsByTagName("head")[0].appendChild(script);
    }else{
      return;
    }
  }
}